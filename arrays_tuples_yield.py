#array - struktura danych, podobna do list do obsługi ciągów / wektorów

#w arrays i lsts jest informacja o porzadku elementow
#w arrays i lists wartośći mogą się powtarzać  [1,1,2]

#w listach w przeciwieństwie do arrays mogą być elementy różnych typów

import array as ar
array1 = ar.array('i', [2,-3, 0, 1, 0])   #i jest to skrot od integer - typ
print(array1)

# array2 = ar.array('i', [2,-3, "0abc", 1]) #podczas uruchomienia tego będzie błąd ze względu na niezgodność typu
# print(array2)

array1.append(-99)  #dodanie elementu na koniec
array1.remove(0)
#array1.append("test")   #niezgodnosc typu
print(array1, len(array1), type(array1))
array1[1] = -1

for el in array1:
    print(el, end = " ")

#odwracanie array  ["abc", "xyz"] - > ["xyz", "abc"]
print("\n", array1[::-1])

import numpy as np
list1 = ["abc", "xyz"]
array2 = np.array(list1)  #array z numpy nie jest immutable
print(array2, type(array2))

array2[0] = 1   #nastapi automatyczna konwersja typu int do str
print(array2, type(array2))

#krotki (tuples)   - ciagi wartości immutable (nie można zmienić wartości krotki ani do niej dodać wartości)
point3d = (-2.5, 0.0, 3.4)
print(point3d, type(point3d), len(point3d))
print(point3d[1])

v1 = (1,2,3);       v2 = (4,5,6)
v3 = v1 + v2  #złączenie dwóch krotek w jedną
# v3[2] = -1 przyklad tego ze nie mozna modyfikowac krotek

print(v3)
print(list(zip(v1, v2)))   #zip zagreguje v1 i v2 do postaci (1, 4), (2, 5), (3, 6)

lst1 = ['a', 'b', 'c']   #liste ktora jest modyfikowalna
tuple1 = tuple(lst1)    #mozna zamienic na niemodyfikalna krotke

list3 = [[(1,2), (3,4)], [("abc"), ("xyz"), ("www")]]   #lista list krotek

#lista list może być reprezentacją tabeli
lst = [[1,"Kasia"],
       [2, "Basia"],
       [3, "Zosia"]]

def proc(name):
    print("hello ", name)    #funkcja ktora nic nie zwraca jest procedura

#yield - produkcja - mechanizmy do generowania kolejnych wartości

def simple_id_generator():
    yield 1
    yield 2
    yield 3
    yield 4

for val in simple_id_generator():    #każda iteracja tej pętli powoduje uzyskanie od generatora kolejnej wartości
    print(val)

for val in simple_id_generator():    #każda iteracja tej pętli powoduje uzyskanie od generatora kolejnej wartości
    print(val)

for val in simple_id_generator():    #każda iteracja tej pętli powoduje uzyskanie od generatora kolejnej wartości
    print(val)

for val in simple_id_generator():    #każda iteracja tej pętli powoduje uzyskanie od generatora kolejnej wartości
    print(val)

print("\n")   #\n jest to symbol białego znaku, inne białe znaki: \t - tabulator

#powyższe 4 wywołania pętli można zapisać krócej:
for i in range(4):    #4x wykonaj:
    for val in simple_id_generator():  # pętla zagnieżdżona
        print(val)

    print("\n")   #po -tym wypisaniu wszystkich wartości generatora za pomocą pętli zagnieżdżonej, wypisujemy separator - "znak entra"