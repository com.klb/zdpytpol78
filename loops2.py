#chcemy zliczyć ilośc wszystkich dzielników liczby n
#np. jeśli n = 6 to jej dzielniki {1,2,3, 6} czyli program powinien wypisać 4
#np: dla 4: 2+1 = 3   {1,2,4}

n = int(input())   #zakładamy że podajemy liczby naturalne >= 1

if (n == 1):   #jeśli to jest prawda to pozostałe warunki nie będą sprawdzane
    print(1)
elif (n == 2):
    print(2)
else:  # czyli n > 2
    div_num = 2  # wynik
    d = 2     #najmniejszy potencjalny dzielnik (liczby > 2)
    half_n = n / 2   #nie ma sensu badać potencjalnych dzielników większych niz połowa n
    while (d <= half_n):   #dopóki kolejny dzielnik jest nie większy niż połowa n
        if (n % d == 0):
            div_num += 1  # inaczej to div_num = div_num + 1

        d += 1  # zwiększy o 1 żeby w kolejnym przebiegu pętli sprawdzić podzielność n przez kolejny potencjalny dzielnik

    print(div_num)





