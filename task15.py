from os.path import exists

def len_of_file(path, word1, word2):
    sum = 0
    with open(path, "r") as file:

        while True:
            line = file.readline()  # czyta kolejną linię
            #zamiast while True mozna uzyc for line in file

            if not line: # czyli natrafił na koniec pliku
 #nie jest potrzebne               file.close()
                return sum #...i zwróci wynik o wartości 0

            if word1 in line or word2 in line:
                if "\n" in line: # jeśli nie jest to ostatnia linia (bo nie ma Entera)
                    sum += len(line) -1
                else:
                    sum += len(line)

print(len_of_file(r"C:\Users\user\PycharmProjects\pycharmdemoproj\zadanie15", "www", "abc"))