# Wypisywanie wartości na standardowe wyjście

#zadanie1--------
print('Python is an interpreted programming language')
print('*****')
print('*   *')
print('*****')
#----------------

#zmienna - fragment (nazwany) / miejsce w pamieci RAM w ktorym monza przechoywac wartosci roznych typow
line = '***********************************'  #deklaracja zmiennej line z przypisaniem wartosci (tekstu)

print(line)
print(line)
print(line)

#x = 1
#print(x)

y = -0.09
p = True     #T and (F or T) -> T
c = 0.5 + 3j
print(y, p, c)

y_type = type(y)
print(y_type)

v1 = 1 + 2*3 + 2**3 - 0.5  + 237/2
print(v1)
v2 = (v1 - 1) * 9.001

message = "the value of variable v2 is {} {}".format(v2, 1)

print(message)
print(message)
print(message)

#zadanie2--------
val1 = -0.003 + (1234 - 321)*(-4) - 3**4

#2 + 4 + 6 +… 996 + 998 + 1000

# 1 + 2 + 3 + 4 + 5 + 6 = 7*6/2

#  2*(1 + 2 + ... 499 + 500) = 2 *
#
val2 = (500/2)*1002
print(val1, val2)

# -----------


# pozostałe typy i operatory

val2 = val2 + 1

print(val2)


#wartości logiczne {True, False}
p = True
q = False
s = p and not (q or p)
print(s)

#reszta z dzielenia
# a = n*b + r    np. 10 = 3*2 + 4     4 to reszta z dzielenia 10 przez 3
# jeśli n % k = 0 to znaczy że n jest podzielne przez k z reszta o (n dzieli sie calkowicie przez k)
print(17 % 2)

#konkatencja
conc = "abc" + "xyz"
print(conc)

#porónywania {==, <, >, ... }

comp_res1 = "Ala1" == "ala1"
print(comp_res1)

z = "1"
v = "3"
#m = v + z + 1  niezgodność typów

comp_res2 = (1 < 2) and (2.3 < 2.4)
print("the value of comparision is {}".format(comp_res2))

#wczytywanie wartości ze standdardowego wejścia
name = input("What is your name?")  #wszystko wczytane z klawiatury za pomoca funkcji input jest napisem (std)
a_std = input("enter the value of a: ")
b_std = input("enter the value of b: ")
a_int = float(a_std)
b_int = float(b_std)
resab = a_int + b_int
print("Hello {} , the sum is {} ".format(name, resab))


