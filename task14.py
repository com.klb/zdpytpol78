def fun_a(nums):
    return sum(list(filter(lambda x : x < 0, nums)))

def fun_b(words):
    return max(map(lambda x: len(x), words))

def tests():
    print(fun_a([2, -3, 4, -1]))
    print(fun_b(["abc", "topologia", "kwant"]))

if __name__ == "__main__":
    tests()

