#wypisanie wszystkicj dzielników n
n = int(input())   #zakładamy że podajemy liczby naturalne >= 1

if (n == 1):   #jeśli to jest prawda to pozostałe warunki nie będą sprawdzane
    print("1")
elif (n == 2):
    print("1, 2")
else:  # czyli n > 2
    d = 2     #najmniejszy potencjalny dzielnik (liczby > 2)
    half_n = n / 2   #nie ma sensu badać potencjalnych dzielników większych niz połowa n
    while (d <= half_n):   #dopóki kolejny dzielnik jest nie większy niż połowa n
        #pojedyncze wykonanie wszystkich instrukcji pętli jest to iteracja
        if (n % d == 0):  #spr. czy d dzieli całkowicie (z reszta 0) n
            print(d, end = ", ")

        d += 1  # zwiększy o 1 żeby w kolejnym przebiegu pętli sprawdzić podzielność n przez kolejny potencjalny dzielnik

    print("{0} {1}".format(1, n));