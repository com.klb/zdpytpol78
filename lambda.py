t1 = (1, 2, 3)   #to moze byc reprezentacja wektora
#        x
t2 = (4, 5, 6)
#        y

print(t1+t2)

#chcemy uzyskać wynik (5, 7, 9)

#funkcja map służy do mapowania - zamiany wartości (która to zamiana jest zdefiniowana w postaci funkcji)
#funkcja lambda jest to funkcja anonimowa (nie ma potrzeby definiowania jej jako ogolno dostepnej w kodzie projektu)
#funkcja map zwróci wynik wyłania funkcji lambda (sumy_ na kolejnych parach dwóch krotek t1 i t2)
#rachunek lambda to podstawa teoretyczna jezyków funkcyjnych
sum_t1t2 = tuple(map(
                lambda x, y: 2*x + y,  #anonimowa funkcja w której definiujemy dowanie
                t1, t2
               ))

print(sum_t1t2)

sum_t1t2_b  = list(map(sum, zip(t1, t2)))   #inna wersja tego co jest powyżej

#powyzsza linie mozna rozlozyc na czesci skladowe:
zip1 = zip(t1, t2)   #obiekt w ktorym sa zagregowabe wartosci
print(zip1, ", ", type(zip1))

map_result1 = map(sum ,zip1)
print(map_result1, ", ", type(map_result1))

list_a = list(map_result1)
print(list_a, ", ", type(list_a))

list5 = [1,2,3, 2, 6, 1, 1]
#od teraz jeśli istnieją gotowe funkcje to należy z nich korzystać zamiast powielać kod
print(sum(list5))
print(max(list5))

mapped = list(map(lambda x: x**2 + 1, list5))  #funkcja map, zadziała funkcją lambda na wszystkich elementach list5
print(mapped)

#korzyścią paradygmatu funkcyjnego (deklaratywnego)  jest skrócenie ilości kodu

list5 = [1,2,3, 2, 6, 1, 1]
def f(lst, a, b):
    temp = list(filter(lambda x : x > a and x < b, list5))   #dla każdego elementu x z list5 jesli należy do przedziału (a, b), dodaj do listy  #2, 3, 2
    return min(temp)   #zwracamy minimum listy temp

#filter służy do selekcji wartości wg. warunku ktory jest zadany przez funkcje lambda a map sluzy do zmiany wartosci

print(f(list5, 1, 5))

#reduce
list6 = [1, 2, 3, 4, 5]
from functools import reduce

product6 = reduce(
                   lambda a, b: a*b,
                   list6)   #nastepuje iteracja po kolejnych elementach list6 i sa liczone kolejne iloczyny. Funkcja lambda bedzie wywolywana dla kolejnych liczb i aktualnego wyniku (iloczynu)
print(product6)


list6 = ["1", "-2", "3", "-4", "5"]
#print(sum(list6))

#funkcyjny sposob dodania wartosci dodatnich dodatnich z list6
sum_list6 = list(
                    filter(
                                lambda x : x >0,
                                map(lambda x : int(x), list6)
                    )
)

#pierwszy sposob uzyskania sumy
print(sum(sum_list6))

#moznia tez sume obliczyc za pomoca reduce
sum_list6b = reduce(
                   lambda a, b: a+b,
                   sum_list6)
print(sum_list6b)


