#W projektach istnieją klasy, z których się korzysta, które nie maja instancji (obiektów)
#Możemy zdefiniować klasę, która jest zbiorem jedynie metod. Metody te mogą być wywoływane z różnych miejsc w projekcie

#Każda klasa powinna być w osobnym module

class StringHelper:

    #chcę żeby była możliwość wywołania poniższej metody bez konieczności robienia obiektu tej klasy - co umożliwia adnotacja @staticmethod
    @staticmethod
    def contains_all(text, words):
        for w in words:
            if w not in text:
                return False

        return True

    @staticmethod
    def simple_concat(words):
        separator = "-"   #obiekt klasy str
        return separator.join(words)     #metoda join rozdieli wszystkie slowa w liscie words separatorem
