def count_char(word, c):
    result = 0

    for x in word:   #interujemy po znakach w slowie (dla kazdej litery)
        if x == c:
            result += 1

    return result

#None jest to pusta wartosc (null) ale to jest cos innego niż pusty napis "", czyli None is not ""
def first_letter(word, letter):
    if word is None or len(word) == 0:
        return False  #return przerywa działanie funkcji

    return word[0] == letter

def count_words(list, word):
    result = 0
    for x in list:
        if x == word:
            result += 1
    return result


#Dzieki ponizszemu warunkowi, jesli uruchomimy ten skrypt to zostanie wypisane "application is running"
#jesli natomiast ten skrypt zostanie zaimportowany, to przy uruchomieniu skryptu importujacego, "application is running"  nie zostanie wypisane
if __name__ == "__main__": #test
    print(count_words(["abc", "ab"], "ab"))