class MusicPlayList:
    songs = set()  #skladowa klasy (nieinstancyjna) na poczatku zbior jest pusty. Klasa MovieChannel agreguje inne obiekty

    def __init__(self, playlist, inital_songs): #channel_name, inital_movies - skladowe instancyjne
        self.playlist = playlist

        self.songs = self.songs.union(inital_songs)  # {a,b} u {x, b, z} = {a, b, x, z}


    def add(self, movie_title):
        self.songs.add(movie_title)

    #metoda zwraca zbior aktualnych filmow
    #Jezeli metoda ma miec dostep jedynie do skladowych klasy (a nie do skladowych instancyjnych) to powinnismy ja adntotowac @classmethod
    #i zamiast self, do skladowych klasy odwolujemy sie za pomoca cls
    @classmethod
    def get_songs(cls):
       # x = cls.channel_name  nie mam dostepu za pomoca cls do skladowej instancyjne
        return cls.songs