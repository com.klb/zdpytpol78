p = True
q = False
s = not q   #   to samo co s = not False   negacja False to True

#operacje: and, or, not
if(1 < 2 and p or not q and (p or q)):   #True and True    or    True and True = True or True = True
    print("test")

if(q and (s or 1 < 2)):
    print("abc")

a = 1 != 2  # a jest rozne od 2
b = 1 == 1
print(a, b)

# p    q
# T or T = T
# T or F = T
# F or T = T
# F or F = F

# p    q
# T and T = T
# T and F = F
# F and T = F
# F and F = F
