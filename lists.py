list1 = [1, 2, 3] #niebezpieczenstwo
list1.append("abc")
print(list1, " type: ", type(list1), len(list1))

values = []
values.append(1.1)
values.append(1.2)
print(values)

if  [1.1, 1.2, 3] == values:
    print("value = [1.1, 1.2]")

#w krotki sposob zainicjalizowac 10 elementowa liste zer
list2 = [0]*10
print(list2)

list3 = [None]*5
print(list3, len(list3))

#List nie są immutable, to znaczy że są modyfikowalne
list1[0] = 2   #nadpisanie wartosci pierwszego elementu nowa wartoscia (2)
print(list1)

list1.remove("abc")
print(list1)

def modify_first_and_append(lst, word, n):
    lst[0] = word      # 1 == 1
    for i in range(1, n+1): #od 1 do n   to samo co range(0, n)
        lst.append(word)

    return lst  #zwracamy referencje na lst / nie kopie

print(modify_first_and_append([1,2,3], "test", 3))  # test, 2, 3, test, test, test

#zamiast zwraca zmieniona liste, mozna ja modyfikowac bezposrednio
def modify_first_and_append_v2(lst, word, n):
    lst[0] = word      # 1 == 1
    for i in range(1, n+1): #od 1 do n   to samo co range(0, n)
        lst.append(word)

list2 = [1,2,3]
modify_first_and_append_v2(list2, "test", 3)  #to niczego nie wypisuje tylko modyfikuje zewnetrzna liste list2
print(list2)


# List comprehensions  (comprehension zdolnosc pojmowania, wyrazenie)
list3 = [i for i in range(10)]
print()    #do listy zostana dodane kolejne wartosci z zakresu od 0 do 9

list4 = [x**3 for x in list3 if x%2 == 0]
print(list4)


#slicing - wygodne uzyskiwanie podciągów    ogólna składnia: [start_index:end_index:step]

print(list3)
print(list3[0:len(list3):2])   #co drugi element od poczatku do konca

#to samo co wyżej można zapisać krócej
print(list3[::2])

print(list3[1:3])  #uzyskujemy podciąg kolejnych wartości od indeksu 1 do indeksu 3-1

#uzyskanie n-tego przedostatniego elementu
print(list3[-3])  #3ci element od konca

print(1)

