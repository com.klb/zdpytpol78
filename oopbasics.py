from commons.string_helper import StringHelper
from task18 import NumberHelper

value = [1,2,3]

print(type(value), value.pop())

num = 1
print(type(num))

#Klasa jest to typ obiektów. Te złożony typ może zawierać definicje składowych klasy.
#Składowa klasy jest to np. pole (zmienna, lista, ...),  funkcja, ...

#Najpier definiuje klasę punktów
class Pair:
    x = 1
    y = 2

    #Każda klasa zawiera (niejawnie) domyslny pusty konstruktor - coś w rodzaju funkcji dzięki której można uzyskać instancję klasy (obiekt)
    def get_str_representation(self):  #dzieki self mam mozliwosc odwolywania sie do skladowych klasy
        return str(self.x) + ", " + str(self.y)

#teraz możemy zrobić obiekt punku
p1 = Pair()   #wywolanie konstruktora spowoduje że w pamięci RAM powstanie konkretny punkt (o "nazwie" p1)
print(p1.get_str_representation())
print(p1.x)

p2 = Pair()   #wywolanie konstruktora spowoduje że w pamięci RAM powstanie konkretny punkt (o "nazwie" p1)

print(type(p2))

class Point3D:
    def __init__(self, x, y, z):  #konstruktor dzieki ktoremy bedzie mozna przekzac trzy wspolrzedne potrzebne aby "zrobić konkretny obiekt tej klasy"
        self.x = x   #"zapamietujemy" przekazane wartosci
        self.y = y   #self.y jest to inna zmienna niz y   sel.y oznacza skladowa klasy Point3D a y oznacza argument konstruktora
        self.z = z

    def get_str_representation(self):  #dzieki self (specjalny argument) mam mozliwosc odwolywania sie do skladowych klasy
        return "({}, {}, {})".format(self.x, self.y, self.z)



p3d1 = Point3D(1, 2, 3)
print(p3d1.z)
p3d1_str = p3d1.get_str_representation()
print(p3d1_str)

#metoda jest to funkcja, która należy do klasy

#wywołanie metody statycznej nie wymaga utworzenia obiektu
print(StringHelper.contains_all("abc xyz www abc", ["xyz", "iii"]))
strhelper = StringHelper()
print(strhelper.simple_concat(["xyz", "iii"]))

print(NumberHelper.max_for_list([1,3,4,6,7,101]))
print(NumberHelper.max_from_two_list([1,2,3,4],[2,3,4,99]))
