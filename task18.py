class NumberHelper:
    @staticmethod
    def max_from_two_list (list1,list2):
        #sposob 1
        # return max(list1 + list2)

        #sposob
        m1 = max(list1)
        m2 = max(list2)

        if m1 > m2:
            return m1

        return m2

        #jak znalezc maximum w bardziej optymalny sposob ?


    @staticmethod
    def max_for_list (list):
        return max(list)
