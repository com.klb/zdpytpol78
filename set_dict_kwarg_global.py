#zbiór

set1 = {"test1", "test2"}
set2 = {"test2", "test1"}

print(set1 == set2)

set3={"test1", "test2", "test1"}
print(set1 == set3)

for el in set3:
    print(el)

set4 = {1}
set4.add(2)
print(set4)
print(len(set4))

set5 = set()
set5.add(-0.0001)
print(set5)

#slownij jest to zbiór par, gdzie każda para ma postać (key, value) - key identufikuje value
dict = {1: "one", 2: "two", 3 : "three" }
print(dict, " ", len(dict))

dict2 =  {"casa": "dom", "negro":"czarny"}

print(dict2["casa"])   #w czasie stalym uzyskujemy wartosc klucza

#mniej istotne rzeczy:

# *varargs - dynamiczne argumenty
def application(*args):
    for a in args:
        print(a)  #aplikacja moze uzywac wartosci argumentow

application("127.00.1", "medium", 0)

print()
#**kwargs - tez totyczy dynamicznej liczby argumentow ale ma postac slownika
def application2(**kwargs):
    print(kwargs.values())

    for key, value in kwargs.items():   #items zwraca zbiór par
        print("{} {}".format(key, value))  #aplikacja moze uzywac wartosci argumentow

application2(ip = "127.0.0.1", user = "Lukasz")
print()
application2(user = "Lukasz", ip = "127.0.0.1")
print()
application2(user = "Lukasz")

c = 1   #ta zmienna jest globalna dla calego skryptu

def add():
    global c  #zeby byla mozliwosc modyfikacji zmiennej globalnej (zadeklarowanej na zewnatrz tej funkcji) trzeba uzyc slowa kluczowego global
    print(c)
    c += 2

add()
print(c)