x_string = input("enter the value of x: ")  #x_string to zmienna typy std (string)
y_string = input("enter the value of y: ")

x_int = float(x_string) #x_int to zmienna typu int (liczby calkowite)
y_int = float(y_string)

# n / 0 = a   n = a * 0

# if(y_int == 0):
#     print("The second value should not be equal 0")
# else:
#     print(x_int / y_int)

#można zapisać to inaczej:
if(y_int != 0):
    print(x_int / y_int)
else:
    print("The second value should not be equal 0")