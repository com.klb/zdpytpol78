from datetime import datetime


def log_message(path, *message):
    current_date_time = datetime.now() #datetime.now() zwraca obiekt
    str_date_time = current_date_time.strftime("%H:%M:%S %d/%m/%Y")
    with open(path, "w") as file:
        for i in message:
            file.write("{} {}\n".format(str_date_time,i))
#log_message(r"C:\Users\kuba2\sdaprojects\PycharmProjects\16092023\data\task_16_a.txt", "System off","System is reparing...", "System is on")

def pow_num_2(path, n):
    with open(path, "w") as file:
        file.write(str([2**i for i in range(1,n+1)]))

# 2, 4, 8, 16, 32, 64, 128       #każda kolejna liczba to poprzednia wymnożona przez 2
# 2**n  =  2**(n-1)*2

#pow_num_2(r"C:\Users\kuba2\sdaprojects\PycharmProjects\16092023\data\task_16_b.txt", 4)


def pow_num_2_optimized(path, n):   #zal n >= 1
    with open(path, "w") as file:
        k = 1
        current_power = 1
        while k <= n:
            current_power *= 2
            k += 1
            file.write(str(current_power) + "\n")

pow_num_2_optimized(r"C:\Users\plbar\PycharmProjects\zdpytpol78\data\task16b.txt", 4)

