from inheritance.noabstractversion.Device import Device


class Laptop(Device):  # klasa MobilePhone dziedziczy po klasie Device, to znaczy że uzyskuje wszystkie składowe klasy bazowej
    def __init__(self, brand, processor="4G"):  #wartosc defaultowa (4g) oznacza ze jesli podczas wywolywania konstruktora nie podam generacji tel. to zostanie ustawienia wartosc 4G
        super().__init__(brand)  # wywołuje konstruktor klasy Device super() oznacza odwolanie sie do skladowej w klazsie wyżej
        self.processor = processor

    def display(self):
        print("Laptop: {} {}".format(self.brand, self.processor))
        if (self.running):
            print("is running")
        else:
            print("is not running")