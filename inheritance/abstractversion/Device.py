from abc import ABC, abstractmethod


class Device(ABC):    #To jest klassa abstrakcyjna ktora nie sluzy do robienia obiektow tylkdo do trzymania wspolnych skladowych dla klas pochodncych
    running = False

    def __init__(self, brand):
        self.brand = brand

    @abstractmethod
    def display(self): #abstrakcyjna metoda oznacza ze nie ma ona implementacji a jej konkretne implementacje musza sie znalezc w klasach pochodnych
        pass  #metoda jest pasywna

    def on_off(self):
        self.running = not self.running



