from inheritance.noabstractversion.Device import Device


class MobilePhone(Device):  # klasa MobilePhone dziedziczy po klasie Device, to znaczy że uzyskuje wszystkie składowe klasy bazowej
    def __init__(self, brand, generation):
        super().__init__(brand)  # wywołuje konstruktor klasy Device
        self.generation = generation

    def display(self):
        print("Mobile phone: {} {}".format(self.brand, self.generation))
        if (self.running):
            print("is running")
        else:
            print("is not running")