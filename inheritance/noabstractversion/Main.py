from inheritance.noabstractversion.Laptop import Laptop
from inheritance.noabstractversion.MobilePhone import MobilePhone

ob1 = MobilePhone("brand1", "10G")

ob1.on_off()   #wowoluje odziedziczona metode on_off
ob1.display()
ob1.on_off()
ob1.display()

ob2 = Laptop("brand2", "1000GHZ")
ob2.on_off()
ob2.display()

#obiekty ob1 i ob2 pasują tez do klasy Device  (sa tego samego typu)
