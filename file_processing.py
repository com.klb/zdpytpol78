#funkcja, która na podstawie argumentu - ścieżki do pliku, zwraca jego zawartość

from os.path import exists

def get_whole_content(path):
    if not exists(path):
        return None     #zwracamy brak wartości - informacje że plik nie istniej. Jesli pliku nie ma to return  przerwie dzialanie funkcji

    file = open(path, "r")   # Deklaruje obiekt za pomoca ktorego będę mógł czytać z plikui (r oznacza tryb czytania z pliku)
    content = file.read()
    file.close()    #nawet gdybysmy nie wywolali close to Python i tak zamknie w pewnym momencie ten plik gdy stwierdzi ze nie jest juz uzywany

    return content

print(get_whole_content(r"C:\Users\plbar\PycharmProjects\zdpytpol78\data\data1.dat")) #r dajemy po to zeby Python ignorował znaki specjalne


#zwraca True jesli dane slowo istnieje w pewnej linii
def chec_if_exists(path, word):
    if not exists(path):
        return None

    file = open(path, "r")
    while True:  #petla teoretycznie nieskonczona
        line = file.readline()    #czytamy kolejna linie
        #sprawdzam czy natrafilem na koniec

        if not line:    #jesli linia nie istnieje (jesli jest koniec pliku)
            file.close()
            return False

        if word in line:    #jesli slowo nalezy do lini (w lini wystepuje slowo)
            file.close()
            return True    #info ze slowo zostalo znalezione


#chcemy zsumować wszystkie liczby z pliku które są oddzielone przecinkami
def sum_numbers(path):
    with open(path, "r") as f:
        line = f.readline()
        str_nums = line.split(",")  #str_nums jest listą liczb w postaci napisów
        return sum(map( lambda s: float(s), str_nums))


def app2():
    # if chec_if_exists(r"C:\Users\plbar\PycharmProjects\zdpytpol78\data\data1.dat", "python"):
    #     print("Podane slowo istnieje w pliku")
    # else:
    #     print("Podane slowo nie istnieje w pliku")

    print(sum_numbers(r"C:\Users\plbar\PycharmProjects\zdpytpol78\data\numbers.txt"))

app2()


w = "123 abc xyz www"
q = "abc" in w and "www" in w
print(q)


#zapisywanie do pliku
#zapisać do pliku kolejne liczby naturalne od 1 do n
def write_to_file(path, n):   #to traktujemy jako procedure
    with open(path, "w") as file:  # w oznacza tryb zapisu
        k = 1
        while k <= n:
            file.write("line {}\n".format(k))
            k += 1

write_to_file(r"C:\Users\plbar\PycharmProjects\zdpytpol78\data\outdata.txt", 10)

from datetime import datetime

def log_message(path, message):
    current_date_time = datetime.now()   #datetime.now()  zwraca obiekt
    str_date_time = current_date_time.strftime("%d/%m/%Y %H:%M:%S")  #konwertujemy obiekt w ktorym jest informacja o dacie i czasie
    with open(path, "w") as file:  # w oznacza tryb zapisu
        file.write("{} {}\n".format(str_date_time, message))

log_message(r"C:\Users\plbar\PycharmProjects\zdpytpol78\data\outdata.txt", "appllication is running")


