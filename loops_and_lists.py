#listy
#lista jest to ciąg wartości. Jesli lista ma n elementów to jej wartości maja kolejne indeksy: 0, 1, ...n-1
# list1 = [1, 2, 3]  - 1,2,3 to sa wartości listy list
#          0  1  2

list1 = [1, 2, 3]
print(list1)

list2 = ['a', 'A', '*', '?']
print(list2)
print(len(list2))

list3 = [1, "word1", -0.001]

list4 = [-0.1, 0, 400, -5, 0, -77]
#chcemy wyświetlić wszytskie wartości ujemne

for val in list4:    #val jest dowolna nazwa zmiennej "interacyjnej" czyli taka która będzie w kolejnych interacjach pętli
    #zawierała kolejne wartości listy list4
    if val < 0:
        print(val, end = " ")

print(list4[1])   #zostanie wyswietlona liczba na pozycji 1 czyli 0


#chcemy wypisać wartości bezwzględne wszystkich liczb z listy list4
#   |x| = x jesli x >=0   albo -x jeśli x < 0

list4_length = len(list4)   #ilosc elementow w liscie czyli 6
print(list4[list4_length - 1])

print('\n')


for i in range(0, list4_length):   #range(0, list4_length) zwraca ciąg liczb 0, 1, 2,  ..., list4_length - 1 czyli w naszym przypadku 0, 1, 2, 3, 4, 5
    if list4[i] < 0.0:
        print(-list4[i])     # to samo -1*list4_length[i]
    else:
        print(list4[i])