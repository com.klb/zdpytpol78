# Zadanie 17
# 1. Napisz klasę Employee. Konstruktor powinien umożliwiać uzyskiwanie instancji tej klasy na podstawie imienia,
# nazwiska, peselu oraz wynagrodzenia. Do klasy dodaj metodę, która zwraca reprezentację tekstową obiektu (podobnie jak w przykładzie)
# 2. Metoda zwracająca reprezentację powinna mieć argument - znak separatora którym mają być oddzielone poszczególne składowe

class Employee:
    def __init__(self, Name, Fam_name, pesel, Salary):
        self.Name = Name
        self. Fam_name = Fam_name
        self.pesel = pesel
        self.Salary = Salary

    def emploee_to_string(self):
        return "{} - {} - {} - {}".format(self.Name, self.Fam_name, self.pesel, self.Salary)

    def emploee_to_string_sep(self, separator):
        return "{0} {4} {1} {4} {2} {4} {3}".format(self.Name, self.Fam_name, self.pesel, self.Salary, separator)

empl1 = Employee("Bartek", "Chrz", 12332343212, 1000)

print(empl1.emploee_to_string())
print(empl1.emploee_to_string_sep("/"))

class Employee:
    def __init__(self, Name, Fam_name, pesel, Salary):
        self.Name = Name
        self. Fam_name = Fam_name
        self.pesel = pesel
        self.Salary = Salary

    def emploee_to_string(self):
        return "{} - {} - {} - {}".format(self.Name, self.Fam_name, self.pesel, self.Salary)

    def emploee_to_string_sep(self, separator):
        return "{0} {4} {1} {4} {2} {4} {3}".format(self.Name, self.Fam_name, self.pesel, self.Salary, separator)

empl1 = Employee("Bartek", "Chrz", 12332343212, 1000)

print(empl1.emploee_to_string())
print(empl1.emploee_to_string_sep("/"))