import os

print("current file: ", __file__)

from pathlib import Path

print("current folder", Path.cwd())

#chcemy wypisać wszystkie pliki z danego katalogu
files = [f for f in os.listdir(r"C:\Users\plbar\PycharmProjects\zdpytpol78") if os.path.isfile(f)]

files_and_folders = [file_or_folder for file_or_folder in os.listdir(r"C:\Users\plbar\PycharmProjects\zdpytpol78") if os.path.isdir(file_or_folder)]

print(files, "\n", files_and_folders)

def remove_filess(folder, file_name):  #procedura do usuwania wszystkich plikow z listy file_name z zadanego folderu
    if os.path.isdir(folder):
        for f in file_name:
            file_path = folder + "\\" + f
            if(os.path.isfile(file_path)):
                os.remove(file_path)

file_name = {"numbers.txt", "outdata2.txt"}
remove_filess(r"C:\Users\plbar\PycharmProjects\zdpytpol78\data_temp", file_name)

import shutil

src_path = r"C:\Users\plbar\PycharmProjects\zdpytpol78\data\numbers.txt"
dst_path = r"C:\Users\plbar\PycharmProjects\zdpytpol78\data_temp"
shutil.copy(src_path, dst_path)