list1 = [-0.1, 0, 400, -5, 0, -77]

#0. Oblicz sumę wszystkich elementów
sum = 0

for el in list1:   #dla każdej liczby z listy list1:
    sum += el   # to samo co sum = sum + el    czyli nowa wartość zmiennej suma to dotychczasowa wartość + kolejna liczba z listy (el)

print(sum)

sum_positive = 0
for el in list1:   #dla każdej liczby z listy list1:
    if el > 0:
        sum_positive  = sum_positive + el

print(sum_positive)

count_negative = 0
for el in list1:   #dla każdej liczby z listy list1:
    if el < 0:
        count_negative += 1

print(count_negative)
