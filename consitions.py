# instrukcje warunkowe
# if wyrażenie którego wynikiem jest False albo True
#   te instrukcje wykonają sie jeśli warunek jest prawdziwy
#

a =  198881

if a % 10 == 7:
    print(a)

x_string = input("enter the value of x: ")
y_string = input("enter the value of y: ")
x_int = int(x_string)
y_int = int(y_string)

if x_int < y_int:
    print("The first number is less than the second one") #ta instrukcją sie wykona gdy wynik porównania x_int < y_int jest True

print("hello")  #to się wykona niezależnie od tego czy warunek x_int < y_int  jest prawdziwy

if x_int < y_int and x_int > 0 and y_int > 0:
    print("The first number is less than the second one and both numbers are positive")
    print("test1")
else:
    print("the condition is False")
    print("test1")


#chcemy wypisać minimum wartości z trzech zmiennych. Np dla 1, 2, 3 wynik to 1, dla 9, 2, 3, wynik 2
a = 5
b = 3
c = 1


if a < b:
    if a < c:
        print(a)  # => a < b and a < c
    else:
        print(c)
elif b < c:
    print(b) # not a < b and b < c
else:
    print(c)  #=> not a < b and not b < c

#rozwiązania równoważne:
# if a < b:
#     if a < c:
#         print(a)
# elif b < c:
#     print(b)
# else:
#     print(c)
#
# if a < b and a < c:
#     print(a)
# elif b < c:
#     print(b)
# else:
#     print(c)


