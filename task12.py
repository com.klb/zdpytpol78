
def negative(lst):
    return [x for x in lst if x < 0]

#przykladowe wywolanie
res = negative([8, -2, 3, -5, -3, -4, -4, 8, -1, -6])
print(res)
#-2, -5, -3, -4, -4, -1, -6

print(res[0:len(res):3])  #użyj slicingu podobnie jak w przykładzie: print(list3[::2])
#bierzemy co trzeci element zaczynajac od indeksu 0 az do indeksu len(res) - 1 (ostatni indeks)

#odnośnie b) (*) - z tego  że zasadnicza funkcja ma zawierać tylko jedną linię - wyrażenie
#nie wynika że w skrypcie - rozwiązaniu zadania ma się zjadować tylko jedna funkcja