import math

#można wywywoływać gotowe funkcje
print(math.pow(2, 3))
print(math.fabs(-1))

#funkcja: f:   X (zbiór - dziedzina) -> Y (przeciwdziedzina, zbiór wartości)


#możemy też definiować własne funkcje
def f(x):
    return x**2  + 1     #x do kwadratu plus jeden

def concat(word1, word2):
    return word1 + "-" + word2

def g(y, z):
    return y + z


#możemy wywoływać nasze funkcje
print(f(1))

v = f(2)
print(v)
print(f(3))

res = concat("abc", "xyz")
print(res)
print(concat("a", "b"))

#przed refaktorem - bez zastosowania funkcji
list = [1,2,3]
sum = 0
for v in list:
    sum += v

print(sum)




list = [1,2,3, 4]
sum2 = 0
for v in list:
    sum2 += v

print(sum2)


list = [1,2,3, 4, 5]
sum3 = 0
for v in list:
    sum3 += v

print(sum3)


#po refaktorze - zastosowanie funkcji
def sum_list(list):
    sum = 0
    for v in list:
        sum += v

    return sum    #zwracamy wynik


print(sum_list([1,2,3]))     #wywołujemy funkcje sum_list, przekazując jej argument - listę [1,2,3],  funkcja zwraca sumę liczb tej listy i
                            #i zwrócony wynik zostaje wyświetlony na ekranie (funkcja print przyjmuje jako argument liczbę i wyświetla na ekranie)

print(sum_list([1,2,3, 4]))
print(sum_list([1,2,3, 4, 5]))