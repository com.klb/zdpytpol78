#pętle

# print(1)
# print(2)
# print(3)
# #...
# print(10)

#zamiast 10 instrukcji, można zastosować pętlę - mechanizm do wykonywania powtórzeń

i = 1   #ta zmienna służy do wyświetlania kolejnych wartości oraz do kontroli czy mamy jeszcze wyświetlić kolejną wartość

while(i <= 10):   #dopóki i jest nie większe niż 10
    print(i, end = ' ')
    i = i + 1    #nowa wartość zmiennej i jest to suma poprzedniej wartości i 1
    #po wykonaniu dwóch powyższych istrukcji, następuje pówrót (pętla) do sprawdzenia czy i <= 10

print("application is running")    #ta isntrukcja jest poza pętlą

# przyklad działania
# i = 1
# ? 1 <= 10
# print(1)
# i =    1 + 1 = 2
# 2 <= 10
# print(2)
# i = 2 + 1 = 3
# ...
# w pewnym momencie na skutek zwiekszania i o 1 i osiągnie wartość 10
# ? 10 <= 10
# print(10)
# i = 10 + 1 = 11
# ? 11 <= 10 - False  czyli pętla przestaje się wykonywać - zostaną wykonane istrukcje za pętlą

while(1 == 1): #przykad pętli nieskończonej. Warunek zawsze prawdziwy
    print(1)


# while(i <= 1000):
#     if(i % 2 ==0):
#         sum += i
#
#     i+=1
#
# print(sum)

#2 + 4 + ... + 1000

sum = 0
i = 2
while (i <= 1000):
    sum += i
    i += 2



