class AdvancedMovieChannel:
    movies = []  #skladowa klasy (nieinstancyjna) na poczatku zbior jest pusty. Klasa MovieChannel agreguje inne obiekty

    def __init__(self, channel_name, inital_movies): #channel_name, inital_movies - skladowe instancyjne
        self.channel_name = channel_name

        for x in inital_movies:
            self.movies.append(x)   # {a,b} u {x, b, z} = {a, b, x, z}

    def add(self, movie_title):
        self.movies.add(movie_title)

    #metoda zwraca zbior aktualnych filmow
    #Jezeli metoda ma miec dostep jedynie do skladowych klasy (a nie do skladowych instancyjnych) to powinnismy ja adntotowac @classmethod
    #i zamiast self, do skladowych klasy odwolujemy sie za pomoca cls
    @classmethod
    def get_movies(cls):
       # x = cls.channel_name  nie mam dostepu za pomoca cls do skladowej instancyjne
        return [mov.title for mov in cls.movies]