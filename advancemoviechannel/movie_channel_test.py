from advancemoviechannel.Movie import Movie
from advancemoviechannel.advanced_movie_channel import AdvancedMovieChannel
from movie_channel import MovieChannel

# mv1 = MovieChannel("algorithms", {"introduction to algorithms", "discret knapsack problem"})
# mv1.add("Djkstra algorithm")
#
# print(mv1.get_movies())

mv2 = AdvancedMovieChannel("channel2", [
                        Movie("title1", "dir1", 80),
                        Movie("title2", "dir2", 70)
                   ])

print(mv2.get_movies())

