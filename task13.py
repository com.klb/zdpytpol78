def sum_tuples(a, b):
    if len(a) != len(b):
        return None

    list=[]
    for x in range(len(a)):
        list.append(a[x] + b[x])

    return list

print(sum_tuples((2.0, 0.0, -3.0, 1.5), (2.3, 2.0, -3.0, 0.0)))

result = sum_tuples((2.0, 0.0, -3.0, 1.5), (2.3, 2.0, -3.0, 0.0, 444.44))
if result:    #skrótowy zapus if result is not None
    print(result)